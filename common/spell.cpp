#include "spell.h"

const QStringList Spell::ClassNames = {"Adept",
                                       "Alchemist",
                                       "Antipaladin",
                                       "Arcanist",
                                       "Bard",
                                       "Bloodrager",
                                       "Cleric",
                                       "Druid",
                                       "Hunter",
                                       "Inquisitor",
                                       "Investigator",
                                       "Magus",
                                       "Medium",
                                       "Mesmerist",
                                       "Occultist",
                                       "Oracle",
                                       "Paladin",
                                       "Psychic",
                                       "Ranger",
                                       "Red Mantis Assassin",
                                       "Sahir Afiyun",
                                       "Shaman",
                                       "Skald",
                                       "Sorcerer",
                                       "Spiritualist",
                                       "Summoner",
                                       "Summoner (Unchained)",
                                       "Warpriest",
                                       "Witch",
                                       "Wizard"};

QDataStream& operator<<(QDataStream& ds, const Source& s) {
  ds << s.book;
  ds << s.page;
  return ds;
}

QDataStream& operator>>(QDataStream& ds, Source& s) {
  ds >> s.book;
  ds >> s.page;
  return ds;
}

static QDataStream& operator<<(QDataStream& ds, const std::array<int, 30>& s) {
  for(int i : s) { ds << i; }
  return ds;
}

static QDataStream& operator>>(QDataStream& ds, std::array<int, 30>& s) {
  for(int& i : s) { ds >> i; }
  return ds;
}

QDataStream& operator<<(QDataStream& ds, const Spell& s) {
  ds << s.name;
  ds << s.description;
  ds << s.arcivesUrl;
  ds << s.costFocus;
  ds << s.costMaterial;
  ds << s.raceOrReligion;
  ds << s.verbal;
  ds << s.material;
  ds << s.somatic;
  ds << s.focus;
  ds << s.devineFocus;
  ds << s.materialOrDevineFocus;
  ds << s.materialDescription;
  ds << s.focusDescription;
  ds << s.savingThrow;
  ds << s.rulesText;
  ds << s.resistance;
  ds << s.school;
  ds << s.subschool;
  ds << s.tags;
  ds << s.level;
  ds << s.area;
  ds << s.castingTime;
  ds << s.range;
  ds << s.target;
  ds << s.duration;
  ds << s.sources;
  return ds;
}

QDataStream& operator>>(QDataStream& ds, Spell& s) {
  ds >> s.name;
  ds >> s.description;
  ds >> s.arcivesUrl;
  ds >> s.costFocus;
  ds >> s.costMaterial;
  ds >> s.raceOrReligion;
  ds >> s.verbal;
  ds >> s.material;
  ds >> s.somatic;
  ds >> s.focus;
  ds >> s.devineFocus;
  ds >> s.materialOrDevineFocus;
  ds >> s.materialDescription;
  ds >> s.focusDescription;
  ds >> s.savingThrow;
  ds >> s.rulesText;
  ds >> s.resistance;
  ds >> s.school;
  ds >> s.subschool;
  ds >> s.tags;
  ds >> s.level;
  ds >> s.area;
  ds >> s.castingTime;
  ds >> s.range;
  ds >> s.target;
  ds >> s.duration;
  ds >> s.sources;
  return ds;
}
