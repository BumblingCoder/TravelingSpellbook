#pragma once

#include <QDataStream>
#include <QMap>
#include <QString>
#include <QUuid>
#include <QVector>

#include <array>

struct Source {
  QString book;
  int page;
};

QDataStream& operator<<(QDataStream& ds, const Source& s);

QDataStream& operator>>(QDataStream& ds, Source& s);

struct Spell {
  using LevelArrayType = std::array<int, 30>;
  QString name;
  QString description;
  QString arcivesUrl;
  bool verbal = false;
  bool costFocus = false;
  bool costMaterial = false;
  bool raceOrReligion = false;
  bool material = false;
  bool somatic = false;
  bool focus = false;
  bool devineFocus = false;
  bool materialOrDevineFocus = false;
  QString materialDescription;
  QString focusDescription;
  QString savingThrow;
  QString rulesText;
  bool resistance = false;
  QString school;
  QString subschool;
  QStringList tags;
  LevelArrayType level = []() {
    LevelArrayType a;
    a.fill(-1);
    return a;
  }();
  QString area;
  QString castingTime;
  QString range;
  QString target;
  QString duration;
  QVector<Source> sources;
  // Matches the order of SpellLevel
  // This is a QStringList so it can be transparently passed to QML.
  static const QStringList ClassNames;
};

QDataStream& operator<<(QDataStream& ds, const Spell& s);

QDataStream& operator>>(QDataStream& ds, Spell& s);

Q_DECLARE_METATYPE(Spell::LevelArrayType)
