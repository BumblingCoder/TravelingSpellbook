#!/bin/bash

wget -q "https://github.com/probonopd/linuxdeployqt/releases/download/5/linuxdeployqt-5-x86_64.AppImage"
chmod a+x linuxdeployqt-5-x86_64.AppImage
./linuxdeployqt-5-x86_64.AppImage --appimage-extract
squashfs-root/AppRun build-linux/release/install-root/usr/local/Spellbook -no-translations -qmldir=Spellbook/qml
