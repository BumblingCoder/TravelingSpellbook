import qbs
import qbs.Process

Project {
    QtApplication {
        name: "Spellbook"

        Depends {
            name: "Qt.quick"
        }
        Depends {
            name: "Qt.svg"
        }

        Properties {
            condition: qbs.targetOS.contains("android")
            Android.sdk.packageName: "com.gitlab.bumblingcoder.travelingspellbook"
            Android.sdk.versionCode: { var p = new Process();
            p.exec( "git", ["rev-list", "HEAD", "--count"] );
            return parseInt(p.readStdOut());
            }
        }

        cpp.cxxLanguageVersion: "c++20"
        cpp.includePaths: '../common'

        Group {
            name: "sources"
            files: ["src/**", "../common/*"]
            excludeFiles: ["src/main/**"]
        }

        Group {
            name: "qml"
            fileTags: "qt.core.resource_data"
            files: ["qml/**", "qtquickcontrols2.conf"]
        }

        Group {
            name: "spellsdb"
            fileTags: "qt.core.resource_data"
            //Wildcard to make it not break Southshould the file not exist.
            files: ["../spells*.db"]
        }

        Group {
            name: "icons"
            fileTags: "qt.core.resource_data"
            files: ["icons/material/*.svg"]
        }

        Group {
            name: "aboutText"
            fileTags: "qt.core.resource_data"
            files: ["AboutPageText.txt", "../LICENSE", "../OGL.txt"]
        }

        Group {
            // Properties for the produced executable
            fileTagsFilter: ["application", "android.apk"]
            qbs.install: true
        }
    }
}
