import QtQuick 2.11
import QtQuick.Controls 2.4

ApplicationWindow {
    id: window
    visible: true
    title: "Traveling Spellbook"

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            icon.source: "baseline-menu-24px.svg"
            onClicked: {
                drawer.open()
            }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
        ToolButton {
            property var menu: stackView.currentItem.menu
            id: menuButton
            //            menu: stackView.currentItem.menu
            anchors.right: parent.right
            icon.source: "baseline-more_vert-24px.svg"
            onClicked: {
                menu.x = x
                menu.open(menuButton)
            }
            visible: menu != null
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height
        onAboutToShow: {
            drawerStack.pop(null)
        }

        StackView {

            id: drawerStack
            anchors.fill: parent
            initialItem: Column {

                ItemDelegate {
                    text: "Spells (All)"
                    width: parent.width
                    onClicked: {
                        stackView.pop(null)
                        stackView.push("SpellsAllPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: "Spells Search"
                    width: parent.width
                    onClicked: {
                        stackView.pop(null)
                        stackView.push("SpellsSearchPage.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: "Spells By Class/Level"
                    width: parent.width

                    onClicked: {
                        drawerStack.push("ClassLevelList.qml")
                    }
                }
                ItemDelegate {
                    text: "About"
                    width: parent.width
                    onClicked: {
                        stackView.pop(null)
                        stackView.push("AboutPage.qml")
                        drawer.close()
                    }
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "HomePage.qml"
        anchors.fill: parent
    }

    onClosing: {
        //a bit strange on desktop, this is how android works.
        if (drawer.visible) {
            close.accepted = false
            drawer.close()
        } else if (stackView.depth > 1) {
            close.accepted = false
            stackView.pop()
        } else {
            return
        }
    }
}
