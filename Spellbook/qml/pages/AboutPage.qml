import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.12

Page {
    title: "About"
    ScrollView {
        anchors.fill: parent
        contentWidth: width
        Label {
            width: parent.width
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            style: Text.RichText
            text: aboutText
            wrapMode: Text.WordWrap
        }
    }
}
