import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    title: "Spells List (all)"

    ListView {
        id: listView
        anchors.fill: parent
        delegate: SpellDelegateShort {
        }
        model: allSpellsModel
    }
}
