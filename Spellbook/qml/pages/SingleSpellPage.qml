import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    Flickable {
        anchors.fill: parent
        contentHeight: contentColumn.height
        Column {
            id: contentColumn
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            SpellDetailsRow {
                Label {
                    text: "School:"
                }
                Label {
                    text: singleSpellProperties.School
                }
                Label {
                    text: "(" + singleSpellProperties.Subschool + ")"
                    font.italic: true
                    visible: singleSpellProperties.Subschool.length !== 0
                }
            }
            SpellDetailsRow {
                Label {
                    text: "Tags:"
                }
                Label {
                    text: singleSpellProperties.Tags.join(", ")
                }
                visible: singleSpellProperties.Tags.join(", ").length != 0
            }

            SpellDetailsRow {
                Label {
                    width: parent.width
                    text: singleSpellProperties.Level
                    wrapMode: Text.WordWrap
                }
            }

            HeaderRow {
                text: "Casting"
            }

            SpellDetailsRow {
                Label {
                    text: "Components:"
                }
                Label {
                    text: "Verbal"
                    visible: singleSpellProperties.Verbal
                }
                Label {
                    text: "Somatic"
                    visible: singleSpellProperties.Somatic
                }
                Label {
                    text: "Focus"
                    visible: singleSpellProperties.Focus
                }
                Label {
                    text: "Material"
                    visible: singleSpellProperties.Material
                }
                Label {
                    text: "Material/Devine Focus"
                    visible: singleSpellProperties.MaterialOrDevineFocus
                }
                Label {
                    text: "Devine Focus"
                    visible: singleSpellProperties.DevineFocus
                }
            }

            SpellDetailsRow {
                Label {
                    text: "Materials:"
                }
                Label {
                    text: singleSpellProperties.MaterialDescription
                }
                visible: singleSpellProperties.MaterialDescription.length !== 0
            }
            SpellDetailsRow {
                Label {
                    text: "Focus:"
                }
                Label {
                    text: singleSpellProperties.FocusDescription
                }
                visible: singleSpellProperties.FocusDescription.length !== 0
            }

            SpellDetailsRow {
                Label {
                    text: "Casting Time:"
                }

                Label {
                    text: singleSpellProperties.CastingTime
                }
            }

            HeaderRow {
                text: "Effect"
            }

            SpellDetailsRow {
                Label {
                    text: "Saving Throw:"
                }

                Label {
                    text: singleSpellProperties.SavingThrow
                }
                visible: singleSpellProperties.SavingThrow.length !== 0
            }
            SpellDetailsRow {
                Label {
                    text: "Resistance:"
                }

                Label {
                    text: singleSpellProperties.Resistance ? "yes" : "no"
                }
            }

            SpellDetailsRow {
                Label {
                    text: "Duration:"
                }
                Label {
                    text: singleSpellProperties.Duration
                }
            }
            SpellDetailsRow {
                Label {
                    text: "Range:"
                }
                Label {
                    text: singleSpellProperties.Range
                }
            }
            SpellDetailsRow {
                Label {
                    text: "Area:"
                }
                Label {
                    text: singleSpellProperties.Area
                }
                visible: singleSpellProperties.Area.length !== 0
            }

            HeaderRow {
                text: "Description"
            }

            SpellDetailsRow {
                id: rulesRow
                Label {
                    width: parent.width
                    text: singleSpellProperties.RulesText
                    wrapMode: Text.WordWrap
                }
            }
        }
    }
    property var menu: Menu {
        Action {
            text: "Open In Browser"
            onTriggered: Qt.openUrlExternally(
                             "https://aonprd.com/" + singleSpellProperties.URL)
        }
    }
}
