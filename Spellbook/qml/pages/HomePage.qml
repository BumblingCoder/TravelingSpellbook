import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    title: "Home"

    Label {
        text: "Welcome to the traveling spellbook."
        anchors.centerIn: parent
    }
}
