import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11
import QtQuick.Controls.Material 2.4

Page {
    title: "Spells Search"

    Rectangle {
        id: searchBackground
        color: Material.background
        height: searchInput.height
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        z: 1
        TextField {
            id: searchInput
            anchors.fill: parent
            placeholderText: "Search"
            onTextChanged: {
                searchByNameModel.setFilterRegExp(text)
            }
        }
    }

    ListView {
        id: listView
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: searchBackground.bottom
        anchors.bottom: parent.bottom
        delegate: SpellDelegateShort {
        }
        model: searchByNameModel
    }
}
