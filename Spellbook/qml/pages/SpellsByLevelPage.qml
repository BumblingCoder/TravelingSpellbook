import QtQuick 2.0
import QtQuick.Controls 2.4

Page {
    property int casterClass
    property int casterLevel

    title: casterClasses[casterClass] + " Spells: "
           + (casterLevel === -1 ? "All" : "Level " + casterLevel)

    ListView {
        anchors.fill: parent
        delegate: SpellDelegateShort {
        }
        model: spellsByLevelModel
    }
}
