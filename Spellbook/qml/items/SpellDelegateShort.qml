import QtQuick 2.11
import QtQuick.Controls 2.4

Item {
    height: row1.height + row2.height + row1.height * .4
    width: parent.width
    Column {
        width: parent.width
        Row {
            width: parent.width
            id: row1
            Label {
                text: Name
                font.bold: true
                font.pointSize: Qt.application.font.pointSize * 1.2
                anchors.verticalCenter: parent.verticalCenter
            }
            spacing: 10
        }
        Row {
            width: parent.width
            id: row2
            Label {
                width: parent.width
                text: Description
                anchors.verticalCenter: parent.verticalCenter
                wrapMode: Text.WordWrap
            }
            spacing: 10
        }
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            singleSpellProperties.setAcceptedRow(SpellIndex)
            stackView.push("SingleSpellPage.qml", {
                               "title": Name
                           })
        }
    }
}
