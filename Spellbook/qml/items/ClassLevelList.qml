import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.12

SubMenu {
    ListView {
        Layout.fillWidth: true
        Layout.fillHeight: true
        model: casterClasses
        delegate: ItemDelegate {
            text: modelData
            width: parent.width
            onClicked: {
                drawerStack.push("LevelMenuList.qml", {
                                     "classIndex": index
                                 })
            }
        }
    }
}
