import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

ColumnLayout {
    ItemDelegate {
        Layout.fillWidth: true
        id: backButton
        icon.source: "baseline-chevron_left-24px.svg"
        text: "Back"
        onClicked: {
            drawerStack.pop()
        }
    }
}
