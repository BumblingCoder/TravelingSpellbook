import QtQuick 2.11
import QtQuick.Controls 2.4

Row {
    id: headerRow
    property string text: ""
    width: parent.width
    Frame {
        padding: 2
        width: parent.width
        Label {
            text: headerRow.text
            anchors.verticalCenter: parent.verticalCenter
            font.bold: true
        }
    }
}
