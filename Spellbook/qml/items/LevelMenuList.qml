import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.12

SubMenu {
    ListView {
        Layout.fillWidth: true
        Layout.fillHeight: true
        model: ["All", "0th (Cantrip)", "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th"]
        delegate: ItemDelegate {
            text: modelData
            width: parent.width
            onClicked: {
                spellsByLevelModel.setTargetClass(classIndex)
                spellsByLevelModel.setLevel(index - 1)

                stackView.pop(null)
                stackView.push("SpellsByLevelPage.qml", {
                                   "casterClass": classIndex,
                                   "casterLevel": index - 1
                               })
                drawer.close()
            }
        }
    }
    property int classIndex: 0
}
