#include <QCommandLineParser>
#include <QDataStream>
#include <QFile>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSortFilterProxyModel>
#include <QSvgRenderer>

#include "../common/spell.h"
#include "levelfilterproxymodel.h"
#include "singleRowProxyModel.h"
#include "spelllistmodel.h"

QString GetAboutText() {
  QFile mainTextFile(":/AboutPageText.txt");
  mainTextFile.open(QFile::ReadOnly);
  QString mainText = mainTextFile.readAll();

  QFile oglTextFile(":/OGL.txt");
  oglTextFile.open(QFile::ReadOnly);
  QString oglText = oglTextFile.readAll();
  oglText.replace("\n", "<br>");

  QFile gplTextFile(":/LICENSE");
  gplTextFile.open(QFile::ReadOnly);
  QString gplText = gplTextFile.readAll();
  gplText.replace("\n", "<br>");

  mainText.replace("[ogl-text]", oglText);
  mainText.replace("[gpl-v3-text]", gplText);

  return mainText;
}

int main(int argc, char* argv[]) {
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QGuiApplication app(argc, argv);

  QCommandLineParser parser;
  parser.addPositionalArgument("SpellsDB", "path to the spells database");
  parser.process(app);

  QString spellsDBLocation = ":/spells.db";
  if(!parser.positionalArguments().empty()) { spellsDBLocation = parser.positionalArguments()[0]; }

  QFile dbFile(spellsDBLocation);
  dbFile.open(QFile::ReadOnly);
  QDataStream ds(&dbFile);
  QVector<Spell> allSpells;
  ds >> allSpells;

  SpellListModel spellsModel(allSpells);
  SingleRowAdapter singleRowProxy(&spellsModel, &spellsModel);
  QSortFilterProxyModel searchByNameModel(&spellsModel);
  searchByNameModel.setSourceModel(&spellsModel);
  searchByNameModel.setFilterCaseSensitivity(Qt::CaseInsensitive);
  LevelFilterProxyModel levelFilter;
  levelFilter.setSourceModel(&spellsModel);

  // Force linking against the SVG library, otherwise androiddeployqt breaks.
  QSvgRenderer r;

  QQmlApplicationEngine engine;
  engine.rootContext()->setContextProperty("allSpellsModel", QVariant::fromValue(&spellsModel));
  engine.rootContext()->setContextProperty("singleSpellProperties",
                                           QVariant::fromValue(&singleRowProxy));
  engine.rootContext()->setContextProperty("searchByNameModel",
                                           QVariant::fromValue(&searchByNameModel));
  engine.rootContext()->setContextProperty("spellsByLevelModel", QVariant::fromValue(&levelFilter));
  engine.rootContext()->setContextProperty("casterClasses", QVariant::fromValue(Spell::ClassNames));
  engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
  engine.rootContext()->setContextProperty("aboutText", GetAboutText());

  if(engine.rootObjects().isEmpty()) return -1;

  return app.exec();
}
