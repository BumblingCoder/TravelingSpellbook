#include "spelllistmodel.h"

class SingleRowAdapter : public QObject {
  Q_OBJECT

  Q_PROPERTY(int acceptedRow READ acceptedRow WRITE setAcceptedRow NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Name READ Name NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Description READ Description NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant URL READ URL NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Verbal READ Verbal NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant CostFocus READ CostFocus NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant RaceOrReligion READ RaceOrReligion NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Material READ Material NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Somatic READ Somatic NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Focus READ Focus NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant DevineFocus READ DevineFocus NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant MaterialOrDevineFocus READ MaterialOrDevineFocus NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant MaterialDescription READ MaterialDescription NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant FocusDescription READ FocusDescription NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant SavingThrow READ SavingThrow NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant RulesText READ RulesText NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Resistance READ Resistance NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant School READ School NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Subschool READ Subschool NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Tags READ Tags NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Level READ Level NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Area READ Area NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant CastingTime READ CastingTime NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Range READ Range NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Target READ Target NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Duration READ Duration NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant Sources READ Sources NOTIFY acceptedRowChanged)
  Q_PROPERTY(QVariant SpellIndex READ SpellIndex NOTIFY acceptedRowChanged)

public:
  SingleRowAdapter(QObject* parent, QAbstractItemModel* sourceModel);
  int acceptedRow() const;

  QVariant Name() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Name);
  }
  QVariant Description() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Description);
  }
  QVariant URL() const { return model->data(model->index(m_acceptedRow, 0), SpellListModel::URL); }
  QVariant Verbal() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Verbal);
  }
  QVariant CostFocus() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::CostFocus);
  }
  QVariant RaceOrReligion() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::RaceOrReligion);
  }
  QVariant Material() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Material);
  }
  QVariant Somatic() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Somatic);
  }
  QVariant Focus() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Focus);
  }
  QVariant DevineFocus() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::DevineFocus);
  }
  QVariant MaterialOrDevineFocus() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::MaterialOrDevineFocus);
  }
  QVariant MaterialDescription() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::MaterialDescription);
  }
  QVariant FocusDescription() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::FocusDescription);
  }
  QVariant SavingThrow() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::SavingThrow);
  }
  QVariant RulesText() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::RulesText);
  }
  QVariant Resistance() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Resistance);
  }
  QVariant School() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::School);
  }
  QVariant Subschool() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Subschool);
  }
  QVariant Tags() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Tags);
  }
  QVariant Level() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Level);
  }
  QVariant Area() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Area);
  }
  QVariant CastingTime() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::CastingTime);
  }
  QVariant Range() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Range);
  }
  QVariant Target() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Target);
  }
  QVariant Duration() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Duration);
  }
  QVariant Sources() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::Sources);
  }
  QVariant SpellIndex() const {
    return model->data(model->index(m_acceptedRow, 0), SpellListModel::SpellIndex);
  }

public slots:
  void setAcceptedRow(int acceptedRow);

signals:
  void acceptedRowChanged(int acceptedRow);

private:
  int m_acceptedRow = 0;
  QAbstractItemModel* model = nullptr;
};
