#include "singleRowProxyModel.h"

SingleRowAdapter::SingleRowAdapter(QObject* parent, QAbstractItemModel* sourceModel)
    : model(sourceModel) {}

int SingleRowAdapter::acceptedRow() const { return m_acceptedRow; }

void SingleRowAdapter::setAcceptedRow(int acceptedRow) {
  if(m_acceptedRow == acceptedRow) return;

  m_acceptedRow = acceptedRow;
  emit acceptedRowChanged(m_acceptedRow);
  const auto roleNames = model->roleNames();
  for(auto role = roleNames.begin(); role != roleNames.end(); ++role) {
    setProperty(role.value(), model->data(model->index(m_acceptedRow, 0), role.key()));
  }
}
