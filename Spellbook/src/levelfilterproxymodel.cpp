#include "levelfilterproxymodel.h"
#include "spelllistmodel.h"

LevelFilterProxyModel::LevelFilterProxyModel(QObject* parent) : QSortFilterProxyModel(parent) {}

bool LevelFilterProxyModel::filterAcceptsRow(int source_row,
                                             const QModelIndex& source_parent) const {
  if(targetClass < 0) return false;
  if(targetClass > 30) return false;
  if(!sourceModel()) return false;

  const auto levelArray =
      sourceModel()
          ->data(sourceModel()->index(source_row, 0, source_parent), SpellListModel::LevelArray)
          .value<Spell::LevelArrayType>();
  const int targetClassLevel = levelArray[targetClass];
  if(targetClassLevel == -1) return false;
  if(targetClassLevel == level) return true;
  if(level == -1) return true;
  return false;
}

void LevelFilterProxyModel::setLevel(int level) {
  this->level = level;
  invalidateFilter();
}

void LevelFilterProxyModel::setTargetClass(int targetClass) {
  this->targetClass = targetClass;
  invalidateFilter();
}
