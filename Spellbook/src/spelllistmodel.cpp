#include "spelllistmodel.h"

SpellListModel::SpellListModel(const QVector<Spell>& spellList, QObject* parent)
    : QAbstractListModel(parent), spells(spellList) {}

int SpellListModel::rowCount(const QModelIndex& parent) const {
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if(parent.isValid()) return 0;

  return spells.size();
}

static QString toString(const std::array<int, 30>& level) {
  QStringList levelStrings;
  for(size_t i = 0; i < level.size(); ++i) {
    if(level[i] != -1) {
      levelStrings += QString("%1 %2").arg(Spell::ClassNames[i], QString::number(level[i]));
    }
  }
  return levelStrings.join(", ");
}

static QStringList toString(const QVector<Source>& sources) {
  QStringList sourcesList;
  for(const auto& source : sources) {
    sourcesList.push_back(QString("%1: pg. %2").arg(source.book, QString::number(source.page)));
  }
  return sourcesList;
}

QVariant SpellListModel::data(const QModelIndex& index, int role) const {
  if(!index.isValid()) return QVariant();

  const auto& spell = spells[index.row()];

  switch(role) {
    case Name: return spell.name;
    case Description: return spell.description;
    case URL: return spell.arcivesUrl;
    case Verbal: return spell.verbal;
    case CostFocus: return spell.costFocus;
    case RaceOrReligion: return spell.raceOrReligion;
    case Material: return spell.material;
    case Somatic: return spell.somatic;
    case Focus: return spell.focus;
    case DevineFocus: return spell.devineFocus;
    case MaterialOrDevineFocus: return spell.materialOrDevineFocus;
    case MaterialDescription: return spell.materialDescription;
    case FocusDescription: return spell.focusDescription;
    case SavingThrow: return spell.savingThrow;
    case RulesText: return spell.rulesText;
    case Resistance: return spell.resistance;
    case School: return spell.school;
    case Subschool: return spell.subschool;
    case Tags: return spell.tags;
    case Level: return toString(spell.level);
    case Area: return spell.area;
    case CastingTime: return spell.castingTime;
    case Range: return spell.range;
    case Target: return spell.target;
    case Duration: return spell.duration;
    case Sources: return toString(spell.sources);
    case SpellIndex: return index.row();
    case LevelArray: return QVariant::fromValue(spell.level);
  }
  return spell.name;
}

QHash<int, QByteArray> SpellListModel::roleNames() const {
  return {{Name, "Name"},
          {Description, "Description"},
          {URL, "URL"},
          {Verbal, "Verbal"},
          {CostFocus, "CostFocus"},
          {RaceOrReligion, "RaceOrReligion"},
          {Material, "Material"},
          {Somatic, "Somatic"},
          {Focus, "Focus"},
          {DevineFocus, "DevineFocus"},
          {MaterialOrDevineFocus, "MaterialOrDevineFocus"},
          {MaterialDescription, "MaterialDescription"},
          {FocusDescription, "FocusDescription"},
          {SavingThrow, "SavingThrow"},
          {RulesText, "RulesText"},
          {Resistance, "Resistance"},
          {School, "School"},
          {Subschool, "Subschool"},
          {Tags, "Tags"},
          {Level, "Level"},
          {Area, "Area"},
          {CastingTime, "CastingTime"},
          {Range, "Range"},
          {Target, "Target"},
          {Duration, "Duration"},
          {Sources, "Sources"},
          {SpellIndex, "SpellIndex"}};
}
