#pragma once

#include <QSortFilterProxyModel>

class LevelFilterProxyModel : public QSortFilterProxyModel {
  Q_OBJECT
  Q_PROPERTY(int level WRITE setLevel);
  Q_PROPERTY(int targetClass WRITE setTargetClass)

public:
  explicit LevelFilterProxyModel(QObject* parent = nullptr);

  bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;

public slots:
  void setLevel(int level);

  void setTargetClass(int targetClass);

private:
  int level;
  int targetClass;
};
