#pragma once

#include <QAbstractListModel>

#include "spell.h"

class SpellListModel : public QAbstractListModel {
  Q_OBJECT

public:
  explicit SpellListModel(const QVector<Spell>& spellList, QObject* parent = nullptr);

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  QHash<int, QByteArray> roleNames() const override;

  enum roles {
    Name = Qt::UserRole,
    Description,
    URL,
    Verbal,
    CostFocus,
    RaceOrReligion,
    Material,
    Somatic,
    Focus,
    DevineFocus,
    MaterialOrDevineFocus,
    MaterialDescription,
    FocusDescription,
    SavingThrow,
    RulesText,
    Resistance,
    School,
    Subschool,
    Tags,
    Level,
    Area,
    CastingTime,
    Range,
    Target,
    Duration,
    Sources,
    SpellIndex,
    LevelArray
  };

private:
  const QVector<Spell>& spells;
};
