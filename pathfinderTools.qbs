import qbs

Project {
    minimumQbsVersion: "1.7.1"
    references: ["Spellbook/Spellbook.qbs", "read-json-spells/read-json-spells.qbs"]
    qbs.installPrefix: ""
}
