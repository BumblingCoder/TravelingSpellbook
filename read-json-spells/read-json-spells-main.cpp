#include <QCommandLineParser>
#include <QCoreApplication>
#include <QFile>
#include <QTimer>
#include <QtDebug>

#include "readspellsfromjson.h"

#include <thread>
int main(int argc, char* argv[]) {

  QCoreApplication a(argc, argv);
  QCommandLineParser parser;
  parser.addPositionalArgument("jsonfile", "json file to read");
  parser.addPositionalArgument("binaryfile", "binary file to write");
  parser.addHelpOption();
  parser.process(a);

  if(parser.positionalArguments().size() != 2) { parser.showHelp(1); }

  QTimer::singleShot(0, [&]() {
    auto spells = readSpellsFromJsonFile(parser.positionalArguments()[0]);

    QFile f(parser.positionalArguments()[1]);
    f.open(QIODevice::WriteOnly);
    QDataStream ds(&f);
    ds << spells;
    a.exit(0);
  });
  return a.exec();
}
