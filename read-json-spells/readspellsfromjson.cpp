#include "readspellsfromjson.h"

#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QRegularExpression>
#include <QtDebug>

static void readDescription(const QJsonObject& spellData, Spell& s) {
  auto descriptionValue = spellData.value("description");
  if(descriptionValue.isUndefined()) {
    qDebug() << s.name << "has no description";
    return;
  }
  s.description = descriptionValue.toString();
}

static void readRules(const QJsonObject& spellData, Spell& s) {
  auto descriptionValue = spellData.value("rulestext");
  if(descriptionValue.isUndefined()) {
    qDebug() << s.name << "has no rules";
    return;
  }
  s.rulesText = descriptionValue.toString();
}

static void readArchivesUrl(const QJsonObject& spellData, Spell& s) {
  auto urlValue = spellData.value("url");
  if(urlValue.isUndefined()) {
    qDebug() << s.name << "has no url";
    return;
  }
  s.arcivesUrl = urlValue.toString();
}

static void readSchool(const QJsonObject& spellData, Spell& s) {
  auto schoolValue = spellData.value("school");
  if(schoolValue.isUndefined()) {
    qDebug() << s.name << "has no school";
    return;
  }

  QRegularExpression schoolExpression("\\w+");
  QRegularExpression subschoolExpression("\\((\\S+)\\)");
  QRegularExpression tagsExpression("\\[(.*)\\]");

  auto schoolString = schoolValue.toString();

  s.school = schoolExpression.match(schoolString).captured();
  s.subschool = subschoolExpression.match(schoolString).captured(1);
  s.tags = tagsExpression.match(schoolString).captured(1).split(", ");

  return;
}

static void readComponents(const QJsonObject& spellData, Spell& s) {
  auto componentsValue = spellData.value("components");
  if(componentsValue.isUndefined()) {
    qDebug() << s.name << "has no components list";
    return;
  }
  QRegularExpression componentDescriptionRegex("\\((.*)\\)");
  auto componentsString = componentsValue.toString();
  for(auto component : componentsString.split(QRegularExpression(", (?![^(]*\\))"))) {
    if(component == "V") {
      s.verbal = true;
    } else if(component == "S") {
      s.somatic = true;
    } else if(component.startsWith("M/DF")) {
      s.materialOrDevineFocus = true;
      auto componentDescription = componentDescriptionRegex.match(component);
      if(componentDescription.hasMatch()) {
        s.materialDescription = componentDescription.captured(1);
      }
    } else if(component.startsWith("M")) {
      s.material = true;
      auto componentDescription = componentDescriptionRegex.match(component);
      if(componentDescription.hasMatch()) {
        s.materialDescription = componentDescription.captured(1);
      }
    } else if(component.startsWith("DF")) {
      s.devineFocus = true;
    } else if(component.startsWith("F")) {
      s.focus = true;
      auto componentDescription = componentDescriptionRegex.match(component);
      if(componentDescription.hasMatch()) { s.focusDescription = componentDescription.captured(1); }
    }
  }

  if(s.focusDescription.contains("worth")) { s.costFocus = true; }
  if(s.materialDescription.contains("worth")) { s.costMaterial = true; }
}

static void readLevel(const QJsonObject& spellData, Spell& s) {
  auto levelValue = spellData.value("level");
  if(levelValue.isUndefined()) {
    qDebug() << s.name << "has no level info";
    return;
  }
  for(const auto levelString : levelValue.toString().split(',')) {
    auto split = levelString.split(' ', QString::SplitBehavior::SkipEmptyParts);
    if(split.size() < 2) continue;
    if(split[0] == "adept") {
      s.level[0] = split[1].toInt();
    } else if(split[0] == "alchemist") {
      s.level[1] = split[1].toInt();
    } else if(split[0] == "antipaladin") {
      s.level[2] = split[1].toInt();
    } else if(split[0] == "arcanist") {
      s.level[3] = split[1].toInt();
    } else if(split[0] == "bard") {
      s.level[4] = split[1].toInt();
    } else if(split[0] == "bloodrager") {
      s.level[5] = split[1].toInt();
    } else if(split[0] == "cleric") {
      s.level[6] = split[1].toInt();
    } else if(split[0] == "druid") {
      s.level[7] = split[1].toInt();
    } else if(split[0] == "hunter") {
      s.level[8] = split[1].toInt();
    } else if(split[0] == "inquisitor") {
      s.level[9] = split[1].toInt();
    } else if(split[0] == "investigator") {
      s.level[10] = split[1].toInt();
    } else if(split[0] == "magus") {
      s.level[11] = split[1].toInt();
    } else if(split[0] == "medium") {
      s.level[12] = split[1].toInt();
    } else if(split[0] == "mesmerist") {
      s.level[13] = split[1].toInt();
    } else if(split[0] == "occultist") {
      s.level[14] = split[1].toInt();
    } else if(split[0] == "oracle") {
      s.level[15] = split[1].toInt();
    } else if(split[0] == "paladin") {
      s.level[16] = split[1].toInt();
    } else if(split[0] == "psychic") {
      s.level[17] = split[1].toInt();
    } else if(split[0] == "ranger") {
      s.level[18] = split[1].toInt();
    } else if(split[0] == "redmantisassassin") {
      s.level[19] = split[1].toInt();
    } else if(split[0] == "sahirafiyun") {
      s.level[20] = split[1].toInt();
    } else if(split[0] == "shaman") {
      s.level[21] = split[1].toInt();
    } else if(split[0] == "skald") {
      s.level[22] = split[1].toInt();
    } else if(split[0] == "sorcerer") {
      s.level[23] = split[1].toInt();
    } else if(split[0] == "spiritualist") {
      s.level[24] = split[1].toInt();
    } else if(split[0] == "summoner" && split[1] != "(unchained)") {
      s.level[25] = split[1].toInt();
    } else if(split[0] == "summoner" && split[1] == "(unchained)") {
      s.level[26] = split[1].toInt();
    } else if(split[0] == "warpriest") {
      s.level[27] = split[1].toInt();
    } else if(split[0] == "witch") {
      s.level[28] = split[1].toInt();
    } else if(split[0] == "wizard") {
      s.level[29] = split[1].toInt();
    } else {
      qDebug() << s.name << "has unknown class" << split[0];
    }
  }
}

static void readArea(const QJsonObject& spellData, Spell& s) {
  auto areaValue = spellData.value("area");
  if(areaValue.isUndefined()) {
    // This isn't unusual
    return;
  }
  s.area = areaValue.toString();
}

static void readCastingTime(const QJsonObject& spellData, Spell& s) {
  auto castTimeValue = spellData.value("castingtime");
  if(castTimeValue.isUndefined()) {
    qDebug() << s.name << "has no casting time info";
    return;
  }
  s.castingTime = castTimeValue.toString();
}

static void readRange(const QJsonObject& spellData, Spell& s) {
  auto rangeValue = spellData.value("range");
  if(rangeValue.isUndefined()) {
    qDebug() << s.name << "has no range info";
    return;
  }
  s.range = rangeValue.toString();
}

static void readTarget(const QJsonObject& spellData, Spell& s) {
  auto targetValue = spellData.value("target");
  if(targetValue.isUndefined()) {
    // perfectly normal
    return;
  }
  s.target = targetValue.toString();
}

static void readSources(const QJsonObject& spellData, Spell& s) {
  auto sourcesValue = spellData.value("sources");
  if(sourcesValue.isUndefined()) {
    qDebug() << s.name << "has no listed sources";
    return;
  }
  for(auto source : sourcesValue.toArray()) {
    Source spellSource;
    auto sourceObject = source.toObject();
    spellSource.book = sourceObject.value("book").toString();
    spellSource.page = sourceObject.value("page").toInt();
    s.sources.push_back(spellSource);
  }
}

static void readDuration(const QJsonObject& spellData, Spell& s) {
  auto durationValue = spellData.value("duration");
  if(durationValue.isUndefined()) {
    qDebug() << s.name << "has no duration";
    return;
  }
  s.duration = durationValue.toString();
}

static void readResistance(const QJsonObject& spellData, Spell& s) {
  auto resistanceValue = spellData.value("resistance");
  if(resistanceValue.isUndefined()) {
    // this isn't unusual
    return;
  }
  s.resistance = resistanceValue.toString().contains("yes");
}

static void readSavingThrow(const QJsonObject& spellData, Spell& s) {
  auto saveValue = spellData.value("savingthrow");
  if(saveValue.isUndefined()) {
    // Perfectly normal
    return;
  }
  s.savingThrow = saveValue.toString();
}

QVector<Spell> readSpellsFromJsonFile(const QString& jsonFile) {
  QFile f(jsonFile);
  f.open(QFile::ReadOnly);
  auto document = QJsonDocument::fromJson(f.readAll());
  QVector<Spell> spellList;
  for(const auto spellName : document.object().keys()) {
    Spell s;
    s.name = spellName;
    auto spellData = document.object().value(spellName).toObject();
    readDescription(spellData, s);
    readRules(spellData, s);
    readArchivesUrl(spellData, s);
    readComponents(spellData, s);
    readLevel(spellData, s);
    readArea(spellData, s);
    readCastingTime(spellData, s);
    readRange(spellData, s);
    readTarget(spellData, s);
    readSchool(spellData, s);
    readSources(spellData, s);
    readDuration(spellData, s);
    readResistance(spellData, s);
    readSavingThrow(spellData, s);
    spellList.push_back(s);
  }
  return spellList;
}
