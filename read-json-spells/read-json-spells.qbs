import qbs

Project {
    minimumQbsVersion: "1.7.1"

    CppApplication {
        Depends {
            name: "Qt.core"
        }

        cpp.cxxLanguageVersion: "c++20"

        consoleApplication: true
        files: ["*", "../common/*"]

        Group {
            // Properties for the produced executable
            fileTagsFilter: "application"
            qbs.install: true
        }
    }
}
