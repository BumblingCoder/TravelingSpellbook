#pragma once

#include <QString>
#include <QVector>

#include "../common/spell.h"

QVector<Spell> readSpellsFromJsonFile(const QString& jsonFile);
