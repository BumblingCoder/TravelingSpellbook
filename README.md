# Traveling Spellbook

A spellbook application. Currently it can be installed on Android and desktop linux. It probably builds fine on windows, but I can't set that up in the build at the moment, so packages for that platform won't be coming for a while.

This app is now available on the google play store!
https://play.google.com/store/apps/details?id=com.gitlab.bumblingcoder.travelingspellbook

This application is governed by the community use guidelines and Open Game License (see OGL.txt)


"This application uses trademarks and/or copyrights owned by Paizo Inc., which are used under Paizo's Community Use Policy. We are expressly prohibited from charging you to use or access this content. This application is not published, endorsed, or specifically approved by Paizo Inc. For more information about Paizo's Community Use Policy, please visit paizo.com/communityuse. For more information about Paizo Inc. and Paizo products, please visit paizo.com."
